<div class="lg:grid grid-cols-2 gap-6">
    <div v-pre>
        <livewire:names-picker name="names" label="Nomes" :names="$grupo->inscritosNaoResponsaveis->pluck('nome')" />
    </div>
    <div>
        <livewire:names-picker name="managers" :label="Sautor\pluralize($grupo->designacao_responsavel)" :names="$grupo->responsaveis->pluck('nome_curto')" />

        <x-forms.input name="date" label="Data" type="date" required />
        <x-forms.input name="priest" label="Celebrante" hint="Separados por ," />
        <x-forms.checkbox name="double" label="Lado a lado A4" id="certificate-double" />
    </div>
</div>
