<div class="field" >
    <label class="label">{{ $label }}</label>
    <div class="control pl-1 flex flex-col gap-2" id="names-controls">
        @foreach($allNames as $idx => $name)
            <div class="checkbox checkbox--small" wire-key="{{ $idx }}">
                <div class="checkbox__input">
                    <input type="checkbox" value="{{ $name }}" id="{{ $inputName }}-{{ $idx }}" wire:model.live="selectedNames">
                </div>
                <div class="checkbox__label">
                    <label for="{{ $inputName }}-{{ $idx }}">{{ $name }}</label>
                </div>
            </div>
        @endforeach

    </div>
    <div class="control">
        <input type="text" class="input input--small" placeholder="Adicionar nome..." wire:model="newName">
        <button type="button" class="button button--small button--icon" wire:click="add">
            <san class="far fa-plus fa-fw"></san>
        </button>
    </div>
    <input type="hidden" name="{{ $inputName }}" value="{{ implode(',', $selectedNames) }}">
</div>
