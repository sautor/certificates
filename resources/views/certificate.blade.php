<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>
        Diploma - {{ config('app.name') }}
    </title>

    <!-- Styles -->
    @vite(['resources/sass/main.scss'])
    <link href="{{ asset('addons/resources/styles.css') }}" rel="stylesheet">
    <link href="{{ asset('addons/certificates/styles.css') }}" rel="stylesheet">
    @include('layouts.partials.custom-styles')
    @stack('styles')
</head>

@php($double = array_key_exists('double', $query))
<body class="{{ implode(" ", $bodyClass) }} @if($double) certificates-double @endif">

@php($names = array_key_exists('names', $query) && trim($query['names']) !== '' ? array_map(fn ($n) => $n === '' ? null : $n ,explode(',', $query['names'])) : [null])

@if($double && count($names) % 2 === 1)
    @php($names[] = null)
@endif

@foreach($names as $name)
    <div class="print-area certificate">
        @unless(empty($grupo->logo) and empty($grupo->icon))
            <div class="certificate__group">
                <img src="{{ url($grupo->icon ?: $grupo->logo) }}">
            </div>
        @endunless

        @php($has_logo = Setting::has('site-logo') or Setting::has('site-logo-invert'))
        @php($image = Setting::get(implode('-', ['addon', 'certificates', $grupo->id, 'setting', 'image'])) ?: Setting::get('grupo-'.$grupo->id.'-hero') ?: (empty(Setting::get('site-hero')) ? null : explode(';', Setting::get('site-hero'))[0]))
        <div class="certificate__image" style="grid-row: {{$grupo->logo ? 2 : 1}} / {{ $has_logo ? 4 : 5 }}; @if($image) background-image: url({{ url($image) }}); @endif ">
        </div>

        @if(Setting::has('site-logo-invert'))
            <div class="certificate__logo certificate__logo--invert">
                <img src="{{ Setting::get('site-logo-invert') }}">
            </div>
        @elseif(Setting::has('site-logo'))
            <div class="certificate__logo">
                <img src="{{ Setting::get('site-logo') }}">
            </div>
        @endif

        <h1 class="certificate__title">
            @php($title = Setting::get(implode('-', ['addon', 'certificates', $grupo->id, 'setting', 'title'])))
            {{ $title }}
        </h1>

        <div class="certificate__main">
            @php($leading = Setting::get(implode('-', ['addon', 'certificates', $grupo->id, 'setting', 'leading'])) ?: 'default')

            {{-- All leading classes: s-crt-prose-tight s-crt-prose-tighter --}}
            <div class="certificate__content s-crt-prose @if($leading !== 'default') s-crt-prose-{{ $leading }} @endif" style="--tw-prose-counters: var(--group-color, var(--primary-500))">
                @php($content = Setting::get(implode('-', ['addon', 'certificates', $grupo->id, 'setting', 'content'])))

                {!! $content !!}
            </div>
        </div>

        <div class="certificate__footer">
            <div class="certificate__footer__main">
                <p class="certificate__byline">
                    @php($prefix = Setting::get(implode('-', ['addon', 'certificates', $grupo->id, 'setting', 'prefix'])))
                    @empty($prefix)
                        Entrega de <strong>{{ $title }}</strong> de
                    @else
                        {!! $prefix !!}
                    @endempty
                </p>
                <p class="certificate__name @if(!$name) certificate__name--empty @endif ">
                    {{ $name }}
                </p>
                <p class="certificate__date">
                    @php($date = array_key_exists('date', $query) ? \Carbon\Carbon::parse($query['date']) : \Carbon\Carbon::now())
                    na {{ config('app.name') }} no dia <strong>{{ $date->isoFormat('LL') }}</strong>
                </p>
            </div>
            <div>
                @php($managers = array_key_exists('managers', $query) && trim($query['managers']) !== '' ? explode(',', $query['managers']) : [])
                <p class="certificate__signature-line">Os {{ \Sautor\pluralize(strtolower($grupo->designacao_responsavel)) }}</p>
                <div class="certificate__signatures certificate__signatures--managers @empty($managers) certificate__signatures--empty @endempty ">
                    @foreach($managers as $manager)
                        <p>{{ $manager }}</p>
                    @endforeach
                </div>
            </div>
            <div class="certificate__signatures-section">
                <p class="certificate__signature-line">O celebrante</p>
                @php($priest = array_key_exists('priest', $query) ? $query['priest'] : null)
                <div class="certificate__signatures certificate__signatures--priest @empty($priest) certificate__signatures--empty @endempty ">
                    @unless(empty($priest))
                        <p>{{ $priest }}</p>
                    @endunless
                </div>
                @if($grupo->pai && $grupo->pai->logo)
                    <img src="{{ url($grupo->pai->logo) }}" class="certificate__parent-logo" />
                @endif
            </div>
        </div>
    </div>
@endforeach

</body>
</html>
