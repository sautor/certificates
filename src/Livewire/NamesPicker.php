<?php

namespace Sautor\Certificates\Livewire;

use Livewire\Component;

class NamesPicker extends Component
{
    public $inputName;

    public $label;

    public $allNames;

    public $selectedNames = [];

    public $newName = '';

    public function mount($name = 'names', $label = 'Nomes', $names = [], $allowEmpty = false)
    {
        $this->inputName = $name;
        $this->label = $label;
        $this->allNames = $names;
    }

    public function add()
    {
        $trimmedName = trim($this->newName);
        if (! empty($trimmedName)) {
            $this->allNames[] = $trimmedName;
            $this->selectedNames[] = $trimmedName;
            $this->newName = '';
        }
    }

    public function render()
    {
        return view('certificates::livewire.names-picker');
    }
}
