<?php

namespace Sautor\Certificates;

use Illuminate\Support\ServiceProvider;
use Livewire\Livewire;
use Sautor\Certificates\Livewire\NamesPicker;
use Sautor\Core\Services\Addons\Addon;
use Sautor\Resources\Resource;

class CertificatesServiceProvider extends ServiceProvider
{
    private Addon $addon;

    /**
     * PaymentsServiceProvider constructor.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     */
    public function __construct($app)
    {
        parent::__construct($app);
        $this->addon = Addon::create('certificates', 'Diplomas', 'file-certificate', 'Extensão com diplomas para os grupos.')
            ->setGroupSettings([
                ['type' => 'input', 'name' => 'title', 'label' => 'Título'],
                ['type' => 'richtext', 'name' => 'content', 'label' => 'Conteúdo'],
                ['type' => 'image', 'name' => 'image', 'label' => 'Imagem'],
                ['type' => 'input', 'name' => 'prefix', 'label' => 'Linha antes do nome'],
                ['type' => 'select', 'name' => 'leading', 'label' => 'Espaçamento entre linhas', 'options' => ['default' => 'Normal', 'tight' => 'Apertado', 'tighter' => 'Mais apertado']],
            ])
            ->withAssets();
    }

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'certificates');

        $addonService = $this->app->make('AddonService');
        $addonService->register($this->addon);

        // Register initial resources
        $this->registerResources();

        Livewire::component('names-picker', NamesPicker::class);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function registerResources()
    {
        $registry = $this->app->make('resources');
        $registry->register(
            Resource::create('diploma', 'Diploma', 'file-certificate', 'certificates::certificate')
                ->setFormat(fn ($query) => array_key_exists('double', $query) ? 'A4' : 'A5')
                ->setGroupResource(true)
                ->setOptionsView('certificates::certificate-options')
                ->setLargeModal(true)
                ->setAddon($this->addon)
        );
    }
}
