import preset from './vendor/sautor/core/tailwind.preset.js'
import typography from '@tailwindcss/typography'

export default {
    presets: [preset],
    prefix: 's-crt-',
    theme: {
      extend: {
          typography: (theme) => ({
              DEFAULT: {
                  css: {
                      fontSize: '9pt',
                      lineHeight: '13pt',
                      p: {
                        marginTop: '10pt',
                        marginBottom: '10pt'
                      },
                      li: {
                          marginTop: '6pt',
                          marginBottom: '6pt',
                      },
                      '> ol > li p, > ul > li p': {
                          marginTop: '6pt',
                          marginBottom: '6pt',
                      },
                      '> ol > li:first-child p, > ul > li:first-child p': {
                          marginTop: '0',
                      },
                      '> ol > li:last-child p, > ul > li:last-child p': {
                          marginBottom: '0',
                      },
                      'ol > li::marker': {
                          fontWeight: '700',
                          color: 'var(--tw-prose-counters)',
                      },
                      'ul > li::marker': {
                          color: 'var(--tw-prose-counters)',
                      },
                  }
              },
              tight: {
                  css: {
                      fontSize: '9pt',
                      lineHeight: '12pt',
                      p: {
                          marginTop: '8pt',
                          marginBottom: '8pt'
                      },
                      li: {
                          marginTop: '6pt',
                          marginBottom: '6pt',
                      },
                      '> ol > li p, > ul > li p': {
                          marginTop: '6pt',
                          marginBottom: '6pt',
                      },
                  }
              },
              tighter: {
                  css: {
                      fontSize: '9pt',
                      lineHeight: '10.5pt',
                      p: {
                          marginTop: '5.5pt',
                          marginBottom: '5.5pt'
                      },
                      li: {
                          marginTop: '5pt',
                          marginBottom: '5pt',
                      },
                      '> ol > li p, > ul > li p': {
                          marginTop: '5pt',
                          marginBottom: '5pt',
                      },
                  }
              }
          })
      }
    },
    plugins: [
        typography
    ]
}
